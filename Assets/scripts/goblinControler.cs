using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goblinControler : MonoBehaviour
{
    public float attackX;
    public float speed;
    public int health;
    public int dammage;
    public float atackSpeed;
    public GameObject attackEffect;
    private ScoreManager scoreManager;
    private bool attacking = false;
    private explosionControler explosion;

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(transform.position.x);
        if(transform.position.x > attackX){
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        } else if(attacking == false) {  
            InvokeRepeating("Attack",0,atackSpeed);
            attacking = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        Debug.Log("Collisoion");
        if(other.gameObject.CompareTag("Explosion")) {
            Debug.Log("Hit");
            explosion = other.gameObject.GetComponent<explosionControler>();
            Debug.Log(explosion.dammage);
            health -= explosion.dammage;
            if(health<=0){
                // Debug.Log("pos");
                // Debug.Log(transform.position.x);
                // Debug.Log("-attackX");
                // Debug.Log(transform.position.x-attackX);
                // Debug.Log("*200");
                // Debug.Log((transform.position.x-attackX)*200f);
                // Debug.Log("+100");
                // Debug.Log(100f+((transform.position.x-attackX)*200f));
                scoreManager.IncreaseScore(100f+((transform.position.x-attackX)*200f));
                Destroy(gameObject);
            }
        }
    }

    void Attack(){
        scoreManager.DecreaseHealth(dammage);
        Instantiate(attackEffect,transform.position,transform.rotation);
    }

}
