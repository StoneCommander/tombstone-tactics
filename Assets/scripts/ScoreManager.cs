using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public float souls = 0;
    public float score = 0;
    public float health = 100;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI soulText;
    public TextMeshProUGUI healthText;
    public GameObject restartButton;
    public TextMeshProUGUI gameOverTxt;

    public void IncreaseScore(float amount){
        score += Mathf.Max(amount);
        UpdateUI();
        // Debug.Log("Score Increased by: " + amount)
    }
    public void IncreaseSouls(int amount){
        souls += amount;
        UpdateUI();
        // Debug.Log("Score Increased by: " + amount)
    }
    public bool SoulTransaction(int cost){
        if(souls>=cost){
            souls-=cost;
            UpdateUI();
            return true;
        } else {
            // add faild transaction popup here
            return false;
        }
    }
    public void DecreaseHealth(int amount){
        health -= amount;
        UpdateUI();
        if(health<=0){
            Time.timeScale = 0;
            gameOverTxt.gameObject.SetActive(true);
            gameOverTxt.text = "Game Over! Final Score: " + score;
            restartButton.SetActive(true);

        }
        // Debug.Log("Score Increased by: " + amount)
    }
    public void ResetGame(){    
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void UpdateUI(){
        scoreText.text = "Score: " + score;
        soulText.text = "Souls: " + souls;
        healthText.text = "Health: " + health;
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateUI();
        gameOverTxt.gameObject.SetActive(false);
        restartButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
