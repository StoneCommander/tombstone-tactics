using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbSpawn : MonoBehaviour
{
    public float spawnInterval;
    public float startDelay;
    public GameObject orb;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnOrb",startDelay,spawnInterval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnOrb() {
        Instantiate(orb,transform.position,transform.rotation);
    }
}
