using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbBehaviour : MonoBehaviour
{
    public float rThrust;
    public float nudgeRange;
    public float nugeThrust;
    public float collectRange;
    private ScoreManager scoreManager;
    private float rawX;
    private float rawY;
    private float dist;
    


    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(Random.Range(-rThrust,rThrust), Random.Range(-rThrust,rThrust)), ForceMode2D.Impulse);
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v3Pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
        v3Pos = Camera.main.ScreenToWorldPoint(v3Pos);
        rawX = v3Pos.x-transform.position.x;
        rawY = v3Pos.y-transform.position.y;
        // Debug.Log(rawX);
        // Debug.Log(rawY);
        dist = Mathf.Sqrt(Mathf.Pow(rawX,2)+Mathf.Pow(rawY,2));
        // Debug.Log(dist);

        if(dist <= collectRange){
            // Debug.Log("Collecnt");
            scoreManager.IncreaseSouls(1);
            Destroy(gameObject);
        }
        if(dist <= nudgeRange){
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(rawX*nugeThrust,rawY*nugeThrust),ForceMode2D.Impulse);
        }
    }
}
