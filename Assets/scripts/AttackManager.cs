using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackManager : MonoBehaviour
{
    public GameObject explosion;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(!Input.GetMouseButtonDown(0)){ 
            Vector3 v3Pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
            v3Pos = Camera.main.ScreenToWorldPoint(v3Pos);
            transform.position = v3Pos;
            transform.position = new Vector3(transform.position.x,transform.position.y,0);
        } else {
            Instantiate(explosion,transform.position,transform.rotation);
            Destroy(gameObject);
        }
    }
}
