using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public float spawnRate;
    public GameObject[] spawnPoints;
    public GameObject goblin; 
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawn",0,spawnRate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawn(){
        int enemyIndex = Random.Range(0,spawnPoints.Length);
        Instantiate(goblin, spawnPoints[enemyIndex].transform.position, spawnPoints[enemyIndex].transform.rotation);
    }
}
