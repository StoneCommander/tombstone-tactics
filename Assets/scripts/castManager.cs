using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class castManager : MonoBehaviour
{
    public GameObject basicCast;
    public GameObject powerCast;
    private ScoreManager scoreManager;
    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void castBasic(){
        if(scoreManager.SoulTransaction(2)){
            Instantiate(basicCast,transform.position,transform.rotation);
        }
    }
    public void castPower(){
        if(scoreManager.SoulTransaction(5)){
            Instantiate(powerCast,transform.position,transform.rotation);
        }
    }
}
