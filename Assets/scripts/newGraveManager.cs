using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newGraveManager : MonoBehaviour
{
    public GameObject newGrave;
    public GameObject button;
    private ScoreManager scoreManager;

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnGrave(){
        if(scoreManager.SoulTransaction(10)){
            Instantiate(newGrave,transform.position,transform.rotation);
            Destroy(button);
            Destroy(gameObject);
        }
    }
}
